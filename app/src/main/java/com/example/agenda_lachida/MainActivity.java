package com.example.agenda_lachida;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView txtNombreAdd,txtNombre1,txtNombre2 ,txtNombre3 ,txtNombre4 ,txtNombre5 ,txtNombre6 ,txtNombre7 ;
    TextView txtTelefonoAdd,txtTelefono1,txtTelefono2,txtTelefono3,txtTelefono4,txtTelefono5,txtTelefono6,txtTelefono7;
    TextView txtMailAdd,txtMail1,txtMail2,txtMail3,txtMail4,txtMail5,txtMail6,txtMail7;
    Button btnAdd;
    ImageButton bnt1, btn2, btn3, btn4, btn5, btn6, btn7;
    int i=3;
    String [] Nombres= new String[20];
    String [] Telefonos= new String[20];
    String [] Mails= new String[20];
    private final int PHONE_CALL_CODE=100;
    private final int CAMERA_CALL_CODE=120;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         txtNombreAdd= findViewById(R.id.txtNombreAdd);
         txtNombre1=  findViewById(R.id.txtNombre1);
         txtNombre2=  findViewById(R.id.txtNombre2);
         txtNombre3=  findViewById(R.id.txtNombre3);
         txtNombre4=  findViewById(R.id.txtNombre4);
         txtNombre5=  findViewById(R.id.txtNombre5);
         txtNombre6=  findViewById(R.id.txtNombre6);
         txtNombre7=  findViewById(R.id.txtNombre7);

         txtTelefonoAdd=  findViewById(R.id.txtNumeroAdd);
         txtTelefono1=  findViewById(R.id.txtTelefono1);
         txtTelefono2=  findViewById(R.id.txtTelefono2);
         txtTelefono3=  findViewById(R.id.txtTelefono3);
         txtTelefono4=  findViewById(R.id.txtTelefono4);
         txtTelefono5=  findViewById(R.id.txtTelefono5);
         txtTelefono6=  findViewById(R.id.txtTelefono6);
         txtTelefono7=  findViewById(R.id.txtTelefono7);

         txtMailAdd=  findViewById(R.id.txtMailAdd);
         txtMail1=  findViewById(R.id.txtMail1);
         txtMail2=  findViewById(R.id.txtMail2);
         txtMail3=  findViewById(R.id.txtMail3);
         txtMail4=  findViewById(R.id.txtMail4);
         txtMail5=  findViewById(R.id.txtMail5);
         txtMail6=  findViewById(R.id.txtMail6);
         txtMail7=  findViewById(R.id.txtMail7);

         btnAdd = findViewById(R.id.button);

        bnt1= (ImageButton) findViewById(R.id.btnLlamar1);
        btn2= (ImageButton) findViewById(R.id.btnLlamar2);
        btn3= (ImageButton) findViewById(R.id.btnLlamar3);
        btn4= (ImageButton) findViewById(R.id.btnLlamar4);
        btn5= (ImageButton) findViewById(R.id.btnLlamar5);
        btn6= (ImageButton) findViewById(R.id.btnLlamar6);
        btn7= (ImageButton) findViewById(R.id.btnLlamar7);

        bnt1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String num= txtTelefono1.getText().toString();
                if (num != null){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, PHONE_CALL_CODE);
                    }else{
                        versionesAnteriores(num);
                    }
                }
            }

            private void versionesAnteriores(String num){
                Intent intentLlamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel"+num));
                if (verificarPermisos(Manifest.permission.CALL_PHONE)){
                    startActivity(intentLlamada);
                }else{
                    Toast.makeText(MainActivity.this, "Acepta los permisos", Toast.LENGTH_SHORT).show();
                }
            }
            private void versionNueva(){

            }
        });

        txtNombre1.setText("Lizeth");
        txtTelefono1.setText("5617736115");
        txtMail1.setText("Mi novia xd ");
        txtNombre2.setText("Billy");
        txtTelefono2.setText("5512234556");
        txtMail2.setText("El que le tiene  miedo a su crush");
        txtNombre3.setText("Alex");
        txtTelefono3.setText("5656565656");
        txtMail3.setText("el mandilon");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PHONE_CALL_CODE:
                String permission = permissions[0];
                int result= grantResults[0];
                if (permission.equals(Manifest.permission.CALL_PHONE)){
                    if (result== PackageManager.PERMISSION_GRANTED){
                        String PhoneNumber= txtTelefono1.getText().toString();
                        Intent llamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+PhoneNumber));
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) return;
                        startActivity(llamada);
                    }else{
                        Toast.makeText(this, "No se acepto el permiso", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case CAMERA_CALL_CODE:
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }

    }

    private boolean verificarPermisos(String permiso){
        int resultado = this.checkCallingOrSelfPermission(permiso);
        return resultado== PackageManager.PERMISSION_GRANTED;
    }
    public void Agregar(View view){
        String Nombre= txtNombreAdd.getText().toString();
        Nombres[i]=Nombre;
        String Telefono= txtTelefonoAdd.getText().toString();
        Telefonos[i]=Telefono;
        String Mail= txtMailAdd.getText().toString();
        Mails[i]=Mail;
        //primer contacto
        txtNombreAdd.setText("");
        txtTelefonoAdd.setText("");
        txtMailAdd.setText("");
        if (txtNombre1.getText()==""){
            txtNombre1.setText(Nombres[i]);
            txtTelefono1.setText(Telefonos[i]);
            txtMail1.setText(Mails[i]);
            Toast.makeText(this, "Contacto 1 Añadido", Toast.LENGTH_SHORT).show();
            i++;
        }
        if (txtNombre1.getText()!=""&&txtNombre2.getText()==""){
            txtNombre2.setText(Nombres[i]);
            txtTelefono2.setText(Telefonos[i]);
            txtMail2.setText(Mails[i]);
            Toast.makeText(this, "Contacto 2 Añadido", Toast.LENGTH_SHORT).show();
            i++;
        }
        if (txtNombre2.getText()!=""&&txtNombre3.getText()==""){
            txtNombre3.setText(Nombres[i]);
            txtTelefono3.setText(Telefonos[i]);
            txtMail3.setText(Mails[i]);
            Toast.makeText(this, "Contacto 3 Añadido", Toast.LENGTH_SHORT).show();
            i++;
        }
        if (txtNombre3.getText()!=""&&txtNombre4.getText()==""){
            txtNombre4.setText(Nombres[i]);
            txtTelefono4.setText(Telefonos[i]);
            txtMail4.setText(Mails[i]);
            Toast.makeText(this, "Contacto 4 Añadido", Toast.LENGTH_SHORT).show();
            i++;
        }
        if (txtNombre4.getText()!=""&&txtNombre5.getText()==""){
            txtNombre5.setText(Nombres[i]);
            txtTelefono5.setText(Telefonos[i]);
            txtMail5.setText(Mails[i]);
            Toast.makeText(this, "Contacto 5 Añadido", Toast.LENGTH_SHORT).show();
            i++;
        }
        if (txtNombre5.getText()!=""&&txtNombre6.getText()==""){
            txtNombre6.setText(Nombres[i]);
            txtTelefono6.setText(Telefonos[i]);
            txtMail6.setText(Mails[i]);
            Toast.makeText(this, "Contacto 6 Añadido", Toast.LENGTH_SHORT).show();
            i++;
        }
        if (txtNombre6.getText()!=""&&txtNombre7.getText()==""){
            txtNombre7.setText(Nombres[i]);
            txtTelefono7.setText(Telefonos[i]);
            txtMail7.setText(Mails[i]);
            Toast.makeText(this, "Contacto 7 Añadido Limite Lleno", Toast.LENGTH_SHORT).show();
        }
    }



}